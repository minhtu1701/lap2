package lap5;

import java.util.ArrayList;


public class lab5_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer("nguyen van teo");

		System.out.println("Length of Name: " + sb.length());
		System.out.println("Last Name = " + sb.substring(0, sb.indexOf(" ")));
		System.out.println("First Name = " + sb.substring(sb.lastIndexOf(" ") + 1));
		System.out.println("Middle Name = " + sb.substring(sb.indexOf(" ") + 1 ,sb.lastIndexOf(" ") + 1));

		String s = "nguyen van teo";
	    StringBuffer res = new StringBuffer();
	    String[] strArr1 = s.split(" ");
        for (String str : strArr1) {
           char[] stringArray = str.trim().toCharArray();
         
           stringArray[0] = Character.toUpperCase(stringArray[0]);
           str = new String(stringArray);
           
           res.append(str).append(" ");
        }
        System.out.println(res);
        
	    StringBuffer res2 = new StringBuffer();
	    String[] strArr2 = s.split(" ");
        for (String str : strArr2) {
           char[] stringArray = str.trim().toCharArray();
           for(int i = 0; i < stringArray.length; i++) {
         	   if(stringArray[i] == 'n') {
         		   stringArray[i] = 'N';
         	   }
            }
           str = new String(stringArray);
           res2.append(str).append(" ");
        }
        System.out.println(res2);
        
        
	    StringBuffer res3 = new StringBuffer();
	    String[] strArr3 = s.split(" ");
        for (String str : strArr3) {

        	String ss = str.toUpperCase();
        	res3.append(ss).append(" ");
        }
        System.out.println(res3);
        
        
	    StringBuffer res4 = new StringBuffer();
	    String[] strArr4 = s.split(" ");
        for (String str : strArr4) {
        	String ss = str.toLowerCase();
        	res4.append(ss).append(" ");
        }
        System.out.print(res4);
	}
}
