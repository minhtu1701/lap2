package lap5;

import java.util.ArrayList;

public class lap5_1 {
	public static void main(String[] args) {
		String s = "nguyen van teo";
		System.out.println("Full Name: " + s);
		System.out.println("Length of Name: " + s.length());
		System.out.println("Last Name = " + s.substring(0, s.indexOf(" ")));
		System.out.println("First Name = " + s.substring(s.lastIndexOf(" ") + 1));
		System.out.println("Middle Name = " + s.substring(s.indexOf(" ") + 1 ,s.lastIndexOf(" ") + 1));
		
		String[] arrWords = s.split(" ");
        ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < arrWords.length; i++) {
			String firstLetter = Character.toString(arrWords[i].charAt(0)).toUpperCase();
			String restWords = arrWords[i].substring(1);
			String word = firstLetter + restWords;
			list.add(word);
		};
		for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
		
		System.out.println("");
		s = "  ly ngoc ";
		System.out.println(s.trim());
		System.out.println(s.replace("n","N"));
		System.out.println(s.toUpperCase());
		System.out.println(s.toLowerCase());


	}
}
