package lap3_1;

public class CanBo extends NhanVien {
	protected String chucvu;
	protected String phongban;
	protected double hesopc;
	
	public CanBo() {
		chucvu = "Truong phong";
		phongban = "Phong hanh chinh";
		hesopc = 5.0;
	}
	
	public CanBo(String ms, String hoten, double hesoluong, String chucvu, double hesopc) {
		super(ms, hoten, hesoluong);
		this.chucvu = chucvu;
		this.hesopc = hesopc;
	}
	
	public CanBo(String ms, String hoten, double hesoluong, String chucvu, String phongban ,double hesopc) {
		super(ms, hoten, hesoluong);
		this.chucvu = chucvu;
		this.hesopc = hesopc;
	}
	
	public String XetThiDua() {
		String XL = "A";
		return XL;
	}
	
	public double phucap() {
		double pc = this.hesopc * luongcoban;
		return pc;
	}

	
	public String GetInfoCB() {
		String infoCB = "ID: " + this.getms() + "- Name: " + this.gethoten()  + "- Chuc vu: " + this.chucvu + "- Phong ban: " + this.phongban + "- Year in: " + this.getnamvaolam() +
						" Days off: " + this.getsongaynghi() + " Phu cap: " + this.phucap() + " Xet thi dua: " + 
						this.XetThiDua() + " Luong = " + this.TinhLuong();
		return infoCB;
	}

	public String getChucvu() {
		return chucvu;
	}

	public void setChucvu(String chucvu) {
		this.chucvu = chucvu;
	}

	public String getPhongban() {
		return phongban;
	}

	public void setPhongban(String phongban) {
		this.phongban = phongban;
	}

	public double getHesopc() {
		return hesopc;
	}

	public void setHesopc(double hesopc) {
		this.hesopc = hesopc;
	}
}
