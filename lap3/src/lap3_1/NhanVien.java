package lap3_1;

import java.time.LocalDate;

public class NhanVien {
	protected String ms;
	protected String hoten;
	protected int namvaolam;
	protected double hesoluong = 1.1;
	static int luongcoban = 1150;
	protected int songaynghi;
	
	public NhanVien() {
		ms = "";
		hoten = "";
		namvaolam = 2015;
		hesoluong = 1.1;
		songaynghi = 0;
	}
	
	public NhanVien(String ms, String hoten, double hesoluong) {
		this.ms = ms;
		this.hoten = hoten;
		this.hesoluong = hesoluong;
	}

	public String getms() {
		return ms;
	}

	public void setId(String ms) {
		this.ms = ms;
	}

	public String gethoten() {
		return hoten;
	}

	public void sethoten(String hoten) {
		this.hoten = hoten;
	}

	public int getnamvaolam() {
		return namvaolam;
	}

	public void setnamvaolam() {
		LocalDate currentDate = LocalDate.now();
		int namvaolam = currentDate.getYear();
		this.namvaolam = namvaolam;
	}
	
	public void setnamvaolam(int namvaolam) {
		this.namvaolam = namvaolam;
	}


	public double gethesoluong() {
		return hesoluong;
	}

	public void sethesoluong(double hesoluong) {
		this.hesoluong = hesoluong;
	}

	public int getsongaynghi() {
		return songaynghi;
	}

	public void setsongaynghi(int songaynghi) {
		this.songaynghi = songaynghi;
	}
	
	public double phucap() {
		LocalDate currentDate = LocalDate.now();
		int sonamlam = currentDate.getYear() - this.namvaolam;
		if(sonamlam >= 5) {
			int phucap = sonamlam * (luongcoban/100);
			return phucap;
		}
		return 0;
	}
	
	public String XetThiDua() {
		String XL = "";
		if(this.songaynghi <= 1) {
			XL = "A";
			return XL;
		} else if(this.songaynghi >= 1 && this.songaynghi <= 3) {
			XL = "B";
			return XL;
		} else {
			XL = "C";
			return XL;
		}
	}
	
	public double TinhLuong() {
		if(this.XetThiDua() == "A") {
			double hs = 1.0;
			double luong = luongcoban * this.hesoluong * hs + this.phucap();
			return luong;
		} else if(this.XetThiDua() == "B") {
			double hs = 0.75;
			double luong = luongcoban * this.hesoluong * hs + this.phucap();
			return luong;
		} else {
			double hs = 0.5;
			double luong = luongcoban * this.hesoluong * hs + this.phucap();
			return luong;
		}
	}
	
	public String GetInfoNV() {
		String infoNV = "ID: " + this.getms() + " Name: " + this.gethoten()  + " Year in: " + this.getnamvaolam() +
						" Days off: " + this.getsongaynghi() + " Phu cap: " + this.phucap() + " Xet thi dua: " + 
						this.XetThiDua() + " Luong = " + this.TinhLuong();
		return infoNV;
	}
	
}
