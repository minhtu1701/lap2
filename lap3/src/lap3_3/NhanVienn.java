package lap3_3;

public class NhanVienn {
	protected String mnv;
	protected String hoten;
	protected double hesoluong;
	public static int luongcoban = 1150;
	
	public NhanVienn() {
		mnv = "NV001";
		hoten = "";
		hesoluong = 2.34;
	}
	
	public NhanVienn(String mnv, String hoten, double hesoluong) {
		this.mnv = mnv;
		String strMNV = mnv;
		boolean flagValid = true;
		
		if(strMNV.length() < 5 || strMNV.length() > 5) {
			flagValid = false;
		}
		
		if(flagValid) {		
			
			if(strMNV.charAt(0) != 'N' && strMNV.charAt(1) != 'V' ) {
				flagValid = false;
			}
			
			String subStr = strMNV.substring(2, 5);
			if(subStr.matches(".*[a-zA-Z].*")) {
				flagValid = false;
			}
			
			this.mnv = mnv;

	    } 
		
		if(!flagValid) {
			this.mnv = "NV001";
		}		
		
		this.hoten = hoten;
		
		this.hesoluong = hesoluong;
	}
	
	public double getThuNhap() {
		double tn = luongcoban * this.getHesoluong();
		return tn;
	}

	public String getMnv() {
		return mnv;
	}

	public void setMnv(String mnv) {
		this.mnv = mnv;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public double getHesoluong() {
		return hesoluong;
	}

	public void setHesoluong(double hesoluong) {
		this.hesoluong = hesoluong;
	}
	
	public void Xuat() {
		System.out.println("Ma nhan vien: " + this.getMnv() + " - Ho ten: " + this.getHoten() + " - He so luong: "
						+ this.getHesoluong() + " - Thu nhap: " + this.getThuNhap());
	}
}

