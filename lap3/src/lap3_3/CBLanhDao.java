package lap3_3;

public class CBLanhDao extends NhanVienn {
	protected String chucvu;
	protected double thamnienql;
	
	public CBLanhDao() {
		mnv = "NV009";
		hoten = "Dieu Hien";
		hesoluong = 4.67;
		chucvu = "Giam doc";
		thamnienql = 10;
	}
	
	public double getThuNhap() {
		double hsld;
		if(this.getChucvu() == "Giam doc") {
			hsld = 7.0;
		} else if (this.getChucvu() == "Truong phong") {
			hsld = 6.0;
		} else if (this.getChucvu() == "Pho phong") {
			hsld = 4.5;
		} else {
			hsld = 1.0;
		}
		double pcld = hsld * 1500;
		double tn = luongcoban * this.getHesoluong() + pcld;
		return tn;
	}

	public String getChucvu() {
		return chucvu;
	}

	public void setChucvu(String chucvu) {
		this.chucvu = chucvu;
	}

	public double getThamnienql() {
		return thamnienql;
	}

	public void setThamnienql(double thamnienql) {
		this.thamnienql = thamnienql;
	}
	
	public void Xuat() {
		System.out.println("Ma nhan vien: " + this.getMnv() + " - Ho ten: " + this.getHoten() + " - He so luong: "
						+ this.getHesoluong() + " - Thu nhap: " + this.getThuNhap() + " - Chuc vu: " + this.getChucvu());
	}
}
