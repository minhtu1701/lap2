package lap3_2;

public class HangHoa {
	protected String mahang;
	protected String tenhang;
	
	public HangHoa() {
		mahang = "";
		tenhang = "";
	}
	
	public HangHoa(String mahang, String tenhang) {
		this.mahang = mahang;
		String strMH = mahang;
		boolean flagValid = true;
		
		if(strMH.length() < 5 || strMH.length() > 5) {
			flagValid = false;
			
		}
		
		if(flagValid) {		
			
			if(strMH.charAt(0) != 'H' || strMH.charAt(1) != 'H' ) {
				flagValid = false;
			}
			
			String subStr = strMH.substring(2, 5);
			if(subStr.matches(".*[a-zA-Z].*")) {
				flagValid = false;
			}
			
			this.mahang = mahang;

	    } 
		
		if(!flagValid) {
			this.mahang = "HH001";
		}
		
		this.tenhang = tenhang;

	}
	
	public void Xuat() {
		System.out.println("Ma hang: " + this.mahang + " - " + "Ten hang: " + this.tenhang);
	}

	public String getMahang() {
		return mahang;
	}

	public void setMaHang(String mahang) {
		String strMH = mahang;
		this.mahang = mahang;

		boolean flagValid = true;
		
		if(strMH.length() < 5 || strMH.length() > 5) {
			flagValid = false;
			
		}
		
		if(flagValid) {		
			
			if(strMH.charAt(0) != 'H' || strMH.charAt(1) != 'H' ) {
				flagValid = false;
			}
			
			String subStr = strMH.substring(2, 5);
			if(subStr.matches(".*[a-zA-Z].*")) {
				flagValid = false;
			}			
				
	    } 
		
		if(!flagValid) {
			this.mahang = "HH001";
		}
	}

	public String getTenhang() {
		return tenhang;
	}

	public void setTenhang(String tenhang) {
		this.tenhang = tenhang;
	}
	
}
