package lap3_2;

public class NGK extends HangHoa {
	protected String donvitinh;
	protected int soluong;
	protected double dongia;
	public static double tilechietkhau = 0.5;
	
	public NGK() {
		mahang = "HH001";
		tenhang = "";
		donvitinh = "Ket";
		soluong = 0;
		dongia = 0;
	}

	
	public NGK(String mahang, String tenhang, String donvitinh, int soluong, double dongia) {
		super(mahang, tenhang);
		if(donvitinh == "Ket" || donvitinh == "Thung" || donvitinh == "Chai" || donvitinh == "Lon") {
			this.donvitinh = donvitinh;
		} else {
			this.donvitinh = "Ket";
		}
		this.soluong = soluong;
		this.dongia = dongia;
	}

	public String getDonvitinh() {
		return donvitinh;
	}

	public void setDonvitinh(String donvitinh) {
		if(donvitinh == "Ket" || donvitinh == "Thung" || donvitinh == "Chai" || donvitinh == "Lon") {
			this.donvitinh = donvitinh;
		} else {
			this.donvitinh = "Ket";
		}
	}

	public int getSoluong() {
		return soluong;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

	public double getDongia() {
		return dongia;
	}

	public void setDongia(double dongia) {
		this.dongia = dongia;
	}
	
	public void Xuat() {
		System.out.println("Ma hang: " + this.getMahang() + " - Ten hang: " + this.getTenhang() + " - Don vi tinh: " 
						+ this.getDonvitinh() +" - So luong: " + this.getSoluong() + " - Don gia: " + this.getDongia());
	}
	
	public double TongTien() {
		double ThanhTien;
		if(this.getDonvitinh() == "Thung" || this.getDonvitinh() == "Ket") {
			ThanhTien = this.getSoluong() * this.getDongia() * tilechietkhau;
		} else if (this.getDonvitinh() == "Chai") {
			ThanhTien = (this.getSoluong() * this.getDongia() / 20) * tilechietkhau;
		} else {
			ThanhTien = (this.getSoluong() * this.getDongia() / 24) * tilechietkhau;
		}
		return ThanhTien;
	}
}
