package lab6_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Array3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap n: ");
		int n = sc.nextInt();
		
		System.out.print("Nhap cac so nguyen : ");
		int [] array = new int[n];
		for(int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();
		}
		
		ArrayList list = new ArrayList<>();
		for(int i = 0; i < array.length; i++) {
			if(isPerfectNumb(array[i])) {
				list.add(array[i]);
			}
		}
		System.out.println("Cac so hoan hao: " + list);
		
		ArrayList list2 = new ArrayList<>();
		for(int i = 0; i < array.length; i++) {
			double sqrtArray = Math.sqrt(array[i]); 
			if(array[i] % sqrtArray == 0 ) {
				list2.add(array[i]);
			}
		}
		System.out.println("Cac so chinh phuong: " + list2);
		Collections.sort(list2);
		int k = list2.size() - 1;
		System.out.println("So chinh phuong lon nhat: " + list2.get(k));
		
		int SumS = 0;
		for(int i = 0; i < list2.size(); i++) {
			int y = (int) list2.get(i);
			SumS += y;
		}
		System.out.println("Tong cac so chinh phuong: " + SumS);
		
		list2.add(1,5);
		System.out.println("Them 5 vao vi tri thu 2" + list2);
		
		list2.remove(0);
		System.out.println("Danh sach con lai sau khi xoa phan tu dau tien: " + list2);
    }
	
	public static boolean isPerfectNumb( int n) {
		int sum = 0;	
		if(n == 0) {
			return false;
		}
        for(int i = 1; i < n; i++)
        {
            if(n % i == 0)
            {
                sum = sum + i;
            }
        }
        if(sum == n)
        {
            return true;
        }
        else
        {
        	return false;
        }    
        
	}	
}