package lab6_2;

import java.util.ArrayList;
import java.util.Scanner;

import java.lang.*;
public class Array2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap n: ");
		int n = sc.nextInt();
		
		System.out.print("Nhap cac so nguyen : ");
		int [] array = new int[n];
		for(int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();
		}
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		
		System.out.println("");

		System.out.println("Cac so nguyen to: ");
		ArrayList list = new ArrayList<>();
		for(int i = 0; i < array.length; i++) {
			if(isPrimeNumber(array[i])) {
				list.add(array[i]);
			}
		}
		System.out.print(list);
		
		
	}
	
	public static boolean isPrimeNumber( int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i < n ; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
	}
}
