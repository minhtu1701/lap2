package lab6_1;

import java.util.ArrayList;
import java.util.Scanner;

public class Array1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap n: ");
		int n = sc.nextInt();
		
		System.out.print("Nhap cac so nguyen : ");
		int [] array = new int[n];
		for(int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();
		}
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		
		System.out.println("");
		System.out.print("Cac so chan trong array: ");
		
		for(int i = 0; i < array.length; i++) {
			if(array[i] % 2 == 0) {
				System.out.print(array[i] + " ");
			}
		}
		
		System.out.println("");
		System.out.print("Phan tu lon nhat trong array: ");
		int max = array[0];
		for(int i = 0; i < array.length; i++) {
			if(array[i] > max) {
				max = array[i];
			}
		}
		System.out.println(max);
		
		System.out.print("Phan tu lon nhat trong array: ");
		int maxEven = array[0];
		for(int i = 0; i < array.length; i++) {
			if(array[i] > maxEven && array[i] % 2 == 0) {
				maxEven = array[i];
			}
		}
		System.out.println(maxEven);
		
		System.out.print("Tong cac phan tu: ");
		int Sum = 0;
		for(int i = 0; i < array.length; i++) {
			Sum += array[i];
		}
		System.out.println(Sum);
	
		ArrayList list = new ArrayList<>();
		System.out.print("Nhap phan tu muon them: ");
		int item = sc.nextInt();
		list.add(item);
		list.add(2);
		list.add(3);
		System.out.println(list);
		System.out.print("Them 5 vao vi tri thu 2");
		list.add(1,5);
		System.out.println(list);

		
	}
}
