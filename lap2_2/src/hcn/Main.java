package hcn;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		hinhchunhat hcn1 = new hinhchunhat();
//		System.out.println("Input cd, cr: ");
//		double cd = sc.nextDouble();
//		double cr = sc.nextDouble();
//		hcn1.setCD(cd);
//		hcn1.setCR(cr);
		System.out.println("HCN 1 :");
		System.out.println("Chu vi = " + hcn1.getCv());
		System.out.println("Dien tich = " + hcn1.getDt());
		System.out.println("Duong cheo = " + hcn1.getDc());
		
		hinhchunhat hcn2 = new hinhchunhat();
		System.out.println("HCN 2 :");
		System.out.println("Chu vi = " + hcn2.getCv(4,5));
		System.out.println("Dien tich = " + hcn2.getDt(4,5));
		System.out.println("Duong cheo = " + hcn2.getDc(4,5));
		System.out.println("Before change:");
		System.out.println(hcn2.getCD());
		System.out.println(hcn2.getCR());
		hcn2.changeSize(10, 5, 1);
		System.out.println("After change:");
		System.out.println(hcn2.getCD());
		System.out.println(hcn2.getCR());
		
		
		
		System.out.println("HCN a:");
		hinhchunhat a = new hinhchunhat(40, 20);
		System.out.println("Before change:");
		System.out.println(a.getCD());
		System.out.println(a.getCR());
		a.changeSize(hcn1, 1);
		System.out.println("After change:");
		System.out.println(a.getCD());
		System.out.println(a.getCR());
		
	}
}

