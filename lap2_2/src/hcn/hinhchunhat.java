package hcn;


public class hinhchunhat {
	private double cd;
	private double cr;
	private double cv;
	private double dt;
	private double dc;


	public hinhchunhat() {
		cd = 20;
		cr = 10;
	}
	
	public hinhchunhat(double cd, double cr) {
		this.cd = cd;
		this.cr = cr;
	}
	
	public void setCD(double cd) {
		this.cd = cd;
	}
	
	
	public double getCD(double cd) {
		return this.cd;
	}
	
	public double getCD() {
		return this.cd;
	}
	
	
	public void setCR(double cr) {
		this.cr = cr;
	}
	
	public double getCR(double cr) {
		return this.cr;
	}
	
	public double getCR() {
		return this.cr;
	}
	
	public double getCv() {
		cv = 2*cd + 2*cr;
		return cv;
	}
	
	public double getCv(double cd, double cr) {
		cv = 2*cd + 2*cr;
		return cv;
	}
	
	
	public double getDt() {
		dt = cd * cr;
		return dt;
	}
	
	public double getDt(double cd, double cr) {
		dt = cd * cr;
		return dt;
	}
	
	
	public double getDc() {
		dc = Math.sqrt(Math.pow(cd,2) + Math.pow(cr,2));
		return dc;
	}
	
	public double getDc(double cd, double cr) {
		dc = Math.sqrt(Math.pow(cd,2) + Math.pow(cr,2));
		return dc;
	}
	
	public void changeSize(double tx, double ty, int kieu) {
		if(kieu == 1) {
			cd = cd + tx;
			cr = cr + ty;
		} else {
			cd = cd - tx;
			cr = cr - ty;
		}
	}
	
	public void changeSize(hinhchunhat hcn,int kieu) {
		if(kieu == 1) {
			double newCD = hcn.getCD() + this.getCD();
			double newCR = hcn.getCR() + this.getCR();
			this.setCD(newCD);
			this.setCR(newCR);

		} else {
			double newCD = hcn.getCD() - this.getCD();
			double newCR = hcn.getCR() - this.getCR();
			this.setCD(newCD);
			this.setCR(newCR);
		}
	}
}
