package Round;

public class Round {
	private double dt;
	private double radius;
	private double cv;
	static double pi = 3.14;
	
	public void setR(double radius ) {
		this.radius = radius;
	}
	
	public double getDt() {
		dt = this.radius*this.radius*pi;
		return dt;
	}
	
	public double getCv() {
		cv = this.radius*2*pi;
		return cv;
	}
	
}
