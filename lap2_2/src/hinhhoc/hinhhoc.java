package hinhhoc;

public abstract class hinhhoc {
	protected int cd;
	protected int cr;
	
	abstract int getCV();
	abstract int getDT();
	
	public hinhhoc(int _cd, int _cr) {
		this.cd = _cd;
		this.cr = _cr;
	}
	
	public int getCD() {
		return cd;
	}
	
	public int getCR() {
		return cr;
	}
	
	public void setCD(int cd) {
		this.cd = cd;
	}
	
	public void setCR(int cr) {
		this.cr = cr;
	}
	
}
