package NhanVien;

public class NhanVien {
	private int ms;
	private String hoten;
	private int songaycong;
	private String[] array = {"Xep loai A","Xep loai B","Xep loai C"};
	private int luong;
	static int luongtrenngay = 200000;
	private int thuong;
	
	public int getNgayCong(int songaycong) {
		return this.songaycong;
	}
	
	public void setNgayCong(int songaycong) {
		this.songaycong = songaycong;
	}
	
	public String xepLoai() {
		if(this.songaycong < 22) {
			return this.array[2];
		} else if (26 >= this.songaycong && this.songaycong>= 22) {
			return this.array[1];
		} else {
			return this.array[0];
		}
	}
	
	
	public int tinhluong() {
		luong = this.songaycong * luongtrenngay;
		return this.luong;
	}
	
	public int thuong() {
		if(this.xepLoai() == this.array[0]) {
			this.thuong = (int) (this.tinhluong() * 0.05);
			return thuong;
		} else if (this.xepLoai() == this.array[1]) {
			this.thuong = (int) (this.tinhluong() * 0.02);
			return thuong;
		} else {
			return this.thuong = 0;
		}
	}
	
}
