package hinhchunhat;

public class hcn {
	private double cd;
	private double cr;
	
	public double getCD(double cd) {
		return this.cd = cd;
	}

	public void setCD(double cd) {
		this.cd = cd;
	}
	
	public double getCR(double cr) {
		return this.cr = cr;
	}

	public void setCR(double cr) {
		this.cr = cr;
	}
	
	public double getCV() {
		double cv = (this.cd + this.cr) * 2;
		return cv;
	}
	
	public double getDT() {
		double dt = this.cd * this.cr;
		return dt;
	}
	
}
