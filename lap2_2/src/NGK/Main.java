package NGK;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String th = sc.nextLine();
		String dvt = sc.nextLine();
		int sl =  sc.nextInt();
		while (sl <= 0) {
			System.out.print("So luong phai lon hon 0.");
			sl = sc.nextInt();
		}
		float dg = sc.nextFloat();
		while (dg <= 0) {
			System.out.print("Don gia phai lon hon 0.");
			dg = sc.nextInt();
		}
		float vat = sc.nextFloat();
		
		NuocGiaiKhat nuocngot = new NuocGiaiKhat(th, dvt, sl, dg, vat);
		
		System.out.println(nuocngot.getNGKInfo());
		System.out.print("Thanh tien: ");
		System.out.print(nuocngot.thanhtien());

	}

}
