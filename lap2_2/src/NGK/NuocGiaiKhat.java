package NGK;

public class NuocGiaiKhat {
	private String th;
	private String dvt;
	private int sl;
	private float dg;
	private float tt;
	static float vat = (float) 0.1;
	public static final String KET = "Ket";
	public static final String THUNG = "Thung";
	public static final String CHAI = "Chai";
	public static final String LON = "Lon";
	
	public NuocGiaiKhat(String th, String dvt, int sl, float dg, float vat ) {
		this.th = th;
		this.dg = dg;
		this.sl = sl;
		if(dvt.equals(KET)) {
			this.dvt = KET;
		} else if (dvt.equals(THUNG)) {
			this.dvt = THUNG;
		} else if(dvt.equals(CHAI)) {
			this.dvt = CHAI;
		} else if( dvt.equals(LON)) {
			this.dvt = LON;
		} else {
			this.dvt = KET;
		}
	}
	
	public String getNGKInfo() {
		String str = "Ten hang: " + this.getTh() + ", " + "don vi tinh: " + this.getDvt() + ", " + "so luong: " 
					 + this.getSl() + ", " + "Don gia: " + this.getDg() + ", " + "Thue VAT: " + this.vat;
		return str;
	}
	
	public String getTh() {
		return th;
	}

	public void setTh(String th) {
		this.th = th;
	}

	public String getDvt() {
		return dvt;
	}

	public void setDvt(String dvt) {
		this.dvt = dvt;
	}

	public int getSl() {
		return sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

	public float getDg() {
		return dg;
	}

	public void setDg(float dg) {
		this.dg = dg;
	}

	public static float getVat() {
		return vat;
	}

	public static void setVat(float vat) {
		NuocGiaiKhat.vat = vat;
	}

	public static String getKet() {
		return KET;
	}

	public static String getThung() {
		return THUNG;
	}

	public static String getChai() {
		return CHAI;
	}

	public static String getLon() {
		return LON;
	}

	public float thanhtien() {
		if(this.dvt == this.KET || this.dvt == this.THUNG) {
			this.tt = this.sl * this.dg*this.vat;
			return tt;
		} else if (this.dvt == this.CHAI) {
			this.tt = this.sl * (this.dg / 20) * this.vat ;
			return tt;
		} else {
			this.tt = this.sl * (this.dg / 24) * this.vat;
			return tt;
		}
	}
}
